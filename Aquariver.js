'use strict';
var express= require('express'),
http=require ('http'),
routes= require('./routes'),
app= express(),
favicon= require('serve-favicon'),
morgan= require('morgan'),
path= require('path'),
redis= require('redis'),
session = require('express-session'),
RedisStore=require('connect-redis')(session),
bodyParser = require('body-parser'),
async = require('async'),
// data={},
server;

app.disable('x-powered-by');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine','jade');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname +'/public/images/ico2.png'));
app.use(morgan('dev'));
app.use(session({
	name: 'Aquariver',
	store: new RedisStore({
		// host: '127.0.0.1',
		db:3
	}),
    secret: 'some string'
}));


function storeData(z){
			console.log(typeof(z))
			console.log(z)
			let tempD=[]
			for (var o in z){
				if (o==="pageUrl") continue;
				console.log(o); 
				tempD.push(o);
				tempD.push(z[o]);
			}
			console.log(tempD)
			return tempD
		
		}

server = http.createServer(app).listen(8123, function(){
	var client= redis.createClient();

//_______________________________________Register
	app.route('/register')
	.get(function(req,res){
		res.render('register',{name: 'registration'})
	})
	
	.post(function(req,res){
		let incomingData=req.body; 
		console.log('req.body ->'+JSON.stringify(req.body)); 
		client.select(1);
		client.exists(req.body.login,function(err,reply){
			if(err){console.log(err)}
			if(reply){res.send('The user with name '+req.body.login+'already exists')}
			else{
				client.hmset(req.body.login,storeData(incomingData),function(err,reply){
					redis.print;
					res.send('user with login name '+req.body.login+'is successfully added')
				});
			}				
		})
	})

//_______________________Log_IN
app.route('/log-in')
	.post(function(req,res){
		client.select(1)
		client.exists(req.body.login,function(err,reply){
			console.log('reply0-> '+JSON.stringify(reply)) 
			if(err){
				console.log(err)
			}
			if(reply){
					client.hmget(req.body.login,'password',function(err,reply){
						console.log('req.body.password-> '+req.body.password) 
						console.log('reply1-> '+JSON.stringify(reply)) 
						if(err) console.log(err)
						if (reply==req.body.password) 
							{client.hgetall(req.body.login, function(err,reply){
								console.log('reply2-> '+JSON.stringify(reply)) 
								req.session.user=reply
								req.session.logged=true
								console.log(req.session.user) 
								res.send('logged in')
							})
							}
						else{
							req.session.destroy()
							res.send('password incorrect')
						}
					})
				}
			else{
						req.session.destroy()
						res.send("there's no user with login name "+req.body.login)
				}
	
		})
	})

//_______________________NEW_PRODUCT
	app.route('/new-product')
	.get(function(req,res){
			console.log('req.session-> '+req.session)
			console.log('req.session.logged-> '+req.session.logged)  
		if(req.session && req.session.logged){
			client.select(1)
			client.exists(req.session.user.login,function(err,reply){
				if(err) console.log(err)
				if(reply){
				res.render('new-product',{name: 'добавление нового товара'})
				} 
				else{
					req.session.destroy()
					res.redirect('/log-in')
				}
			})
		}
		else{res.redirect('/log-in')}
	})

	//___________________________________P____O____S___T
	.post(function(req,res){
		
		var incomingData=req.body
		console.log(typeof incomingData)
		console.log("incomingData.pageUrl1-> "+incomingData.pageUrl);
		client.select(0);
		//добавление данных в бд
	

		client.hmset(incomingData.pageUrl,storeData(incomingData),redis.print);
		console.log(req.body);
		res.send('товар '+req.body.productName+' добавлен в базу данных');
	});

	//______________________G____E_ ___T
	app.get('/:page?', function(req, res){
		client.select(0);
		var page =req.params.page;
			if(!page) {
				page = 'home';
		  		res.render('index', {});
			}
			

			function getData(pge) {
				client.hgetall(pge,function(err,data){
					console.log('data'+JSON.stringify(data))
					res.render('product_page',data)
				})
			}
			

			getData(page)
		});
	

	console.log('server running at port 8123');
});

 