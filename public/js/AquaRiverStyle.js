console.log(window);
console.log(window.screen.availWidth);
console.log(innerWidth);
console.log(innerHeight);

var styleElt, styleSheet;
//var mainWidth = window.screen.width;
//var mainHeight = window.screen.availHeight;
var mainWidth = innerWidth;
var mainHeight = innerHeight;
var head = document.getElementsByTagName("head")[0];
styleElt = document.createElement("style");  // Но­вый эле­мент <style>
head.appendChild(styleElt);// Вста­вить в <head>
// Те­перь но­вая таб­ли­ца на­хо­дит­ся в кон­це мас­си­ва
styleSheet = document.styleSheets[document.styleSheets.length-1];

console.log(styleSheet.cssRules.length);

//установка свойств css относительно ШИРИНЫ окна
function FontSizePresetW(identifyer,stylerule,FontSizeValue ){
var ruleCSS = identifyer + "{"+stylerule+ mainWidth*FontSizeValue +"px}";
styleSheet.insertRule(ruleCSS,styleSheet.cssRules.length);
};

//установка свойств css относительно Высоты окна
function FontSizePresetZ(identifyer,stylerule,FontSizeValue ){
var ruleCSS = identifyer + "{"+stylerule+ mainHeight*FontSizeValue +"px}";
styleSheet.insertRule(ruleCSS,styleSheet.cssRules.length);
};

//____________Front_Page
FontSizePresetZ(".contacts","margin-top:", 0.213);
FontSizePresetW(".categoryLink","font-size:", 0.035);
FontSizePresetZ(".catalog","margin-top:", 0.38);
FontSizePresetZ(".other","margin-top:", 0.753);
FontSizePresetW(".name","font-size:", 0.03);
FontSizePresetW(".categoryLinkLvl2","font-size:", 0.02);
FontSizePresetW(".categoryLinkLvl3","font-size:", 0.015);
FontSizePresetW(".main-img","height:", 0.138);
FontSizePresetZ(".productLink","font-size:", 0.07);

//_________product_page
FontSizePresetZ(".wrapper-product-page","margin-top:", 0.235);
FontSizePresetW(".wrapper-product-page","width:", 0.64);
FontSizePresetW(".sets-of-icons","height:", 0.138);
FontSizePresetW(".sets-of-icons","padding-left:", 0.03);
FontSizePresetW(".sets-of-icons","padding-right:", 0.03);
FontSizePresetW(".scroll-up","margin-left:", 0.007);
FontSizePresetW(".scroll-down","margin-left:", 0.007);
FontSizePresetW(".presetImgItem","width:", 0.05);
FontSizePresetZ(".presetImgItem","height:", 0.05);
FontSizePresetZ("#icons-container li","margin-bottom:", 0.017);

//_________product_page___subsidearies

FontSizePresetZ(".elements-scrollable img","height:", 0.12);
FontSizePresetW(".product-sub","font-size:", 0.012);
FontSizePresetW(".scroll-right","left:", 0.629);
console.log(document.styleSheets);